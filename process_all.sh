#!/bin/bash

y1=2007
y2=2017

for longitude in {-3..-2}
do
	for latitude in {10..11}
	do
		echo $longitude
		echo $latitude
		python ~/DATA/biota/cli/property.py -lat $latitude -lon $longitude -dir ~/DATA/mole/DATA -od ~/DATA/mole/DATA -y ${y1} -o AGB
		python ~/DATA/biota/cli/change.py -lat $latitude -lon $longitude -dir ~/DATA/mole/DATA -od ~/DATA/mole/DATA -y1 ${y1} -y2 ${y2} -ft 10 -ct 0.5 -mt 5 -it 20 
	done
done

# Mosaic tiles

gdal_merge.py -o AGB_${y1}.tif -of GTIff -co COMPRESS=LZW ~/DATA/mole/DATA/AGB_${y1}*.tif
gdal_merge.py -o ChangeType_${y1}_${y2}.tif -of GTIff -co COMPRESS=LZW ~/DATA/mole/DATA/ChangeType_${y1}_${y2}*.tif
